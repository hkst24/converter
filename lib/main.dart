import 'package:flutter/cupertino.dart';

void main() {
  runApp(const Converterapp());
}

class Converterapp extends StatelessWidget {
  const Converterapp({super.key});

  @override
  Widget build(BuildContext context) {
    return CupertinoApp(
      home: MainPage(),
    );
  }
}

class UnitCategory {
  final String name;
  final IconData icon;
  final List<String> units;

  UnitCategory({required this.name, required this.icon, required this.units});
}

class UnitConverterPage extends StatefulWidget {
  final UnitCategory category;

  const UnitConverterPage({super.key, required this.category});

  @override
  // ignore: library_private_types_in_public_api
  _UnitConverterPageState createState() => _UnitConverterPageState();
}

// ignore: use_key_in_widget_constructors
class MainPage extends StatelessWidget {
  final List<UnitCategory> categories = [
    UnitCategory(
      name: 'Длина', icon: CupertinoIcons.arrow_up_down, units: ['Миллиметр', 'Сантиметр', 'Метр', 'Километр', 'Дюйм', 'Фут'],
    ),
    UnitCategory(
      name: 'Валюта', icon: CupertinoIcons.money_dollar_circle, units: ['Рубль', 'Доллар', 'Евро'],
    ),
    UnitCategory(
      name: 'Площадь', icon: CupertinoIcons.square_grid_2x2, units: ['Квадратный миллиметр', 'Квадратный сантиметр', 'Квадратный метр', 'Квадратный киллометр', 'Гектар'],
    ),
    UnitCategory(
      name: 'Данные', icon: CupertinoIcons.folder_circle, units: ['Бит', 'Байт', 'Килобайт', 'Мегабайт', 'Гигабайт', 'Терабайт', 'Петабайт'],
    ),
    UnitCategory(
      name: 'Вес', icon: CupertinoIcons.circle_grid_hex, units: ['Грамм', 'Килограмм', 'Тонна', 'Фунт'],
    ),
    UnitCategory(
      name: 'Время', icon: CupertinoIcons.clock, units: ['Секунда', 'Минута', 'Час'],
    ),
    UnitCategory(
      name: 'Объем', icon: CupertinoIcons.cube_box, units: ['Литр', 'Миллилитр', 'Галлон', 'Кубический сантиметр', 'Кубический метр'],
    ),
    UnitCategory(
      name: 'Скорость', icon: CupertinoIcons.speedometer, units: ['Метр в секунду', 'Километр в час', 'Миля в час'],
    ),
  ];

  @override
    Widget build(BuildContext context) {
      return CupertinoApp(
        home: CupertinoPageScaffold(
          navigationBar: const CupertinoNavigationBar(
            middle: Text('Конвертер единиц измерения'),
          ),
          child: ListView.builder(
            itemCount: categories.length,
            itemBuilder: (context, index) {
              return Padding(
                padding: const EdgeInsets.all(8.0),
                child: CupertinoButton.filled(
                  child: Row(
                    children: <Widget>[
                      Icon(categories[index].icon, size: 50),
                      const SizedBox(width: 16),
                      Text(categories[index].name, style: const TextStyle(fontSize: 24)),
                    ],
                  ),
                  onPressed: () {
                    Navigator.push(
                      context,
                      CupertinoPageRoute(
                        builder: (context) => UnitConverterPage(category: categories[index]),
                      ),
                    );
                  },
                ),
              );
            },
          ),
        ),
      );
    }
}

class _UnitConverterPageState extends State<UnitConverterPage> {
  String selectedUnit = '';
  String inputValue = '';
  Map<String, double> convertedValues = {};

  @override
  void initState() {
    super.initState();
    selectedUnit = widget.category.units[0];
  }

  void convertValue() {
    setState(() {
      double? parsedValue = double.tryParse(inputValue);
      if (parsedValue != null) {
        convertedValues = _calculateConversion(parsedValue, selectedUnit);
      } else {
        convertedValues = {};
      }
    });
  }

  Map<String, double> _calculateConversion(double value, String unit) {
    final conversionFactors = _getConversionFactors();
    final unitConversions = conversionFactors[unit];
    if (unitConversions != null) {
      return unitConversions.map((unit, factor) => MapEntry(unit, value * factor));
    }
    return {};
  }

  Map<String, Map<String, double>> _getConversionFactors() {
    return {
          'Миллиметр': {'Сантиметр': 0.1, 'Метр': 0.001, 'Километр': 0.000001, 'Дюйм': 0.0393701, 'Фут': 0.00328084},
          'Сантиметр': {'Миллиметр': 10, 'Метр': 0.01, 'Километр': 0.00001, 'Дюйм': 0.393701, 'Фут': 0.0328084},
          'Метр': {'Миллиметр': 1000, 'Сантиметр': 100, 'Километр': 0.001, 'Дюйм': 39.3701, 'Фут': 3.28084},
          'Километр': {'Миллиметр': 1000000, 'Сантиметр': 100000, 'Метр': 1000, 'Дюйм': 39370.1, 'Фут': 3280.84},
          'Дюйм': {'Миллиметр': 25.4, 'Сантиметр': 2.54, 'Метр': 0.0254, 'Километр': 0.0000254, 'Фут': 0.0833333},
          'Фут': {'Миллиметр': 304.8, 'Сантиметр': 30.48, 'Метр': 0.3048, 'Километр': 0.0003048, 'Дюйм': 12},

          'Рубль': {'Доллар': 0.01069, 'Евро': 0.01005},
          'Доллар': {'Рубль': 93.57, 'Евро': 0.94},
          'Евро': {'Рубль': 99.50, 'Доллар': 1.06},

          'Квадратный миллиметр': {'Квадратный сантиметр': 0.01, 'Квадратный метр': 0.000001, 'Квадратный киллометр': 1e-12, 'Гектар': 1e-10},
          'Квадратный сантиметр': {'Квадратный миллиметр': 100, 'Квадратный метр': 0.0001, 'Квадратный киллометр': 1e-10, 'Гектар': 1e-8},
          'Квадратный метр': {'Квадратный миллиметр': 1000000, 'Квадратный сантиметр': 10000, 'Квадратный киллометр': 0.000001, 'Гектар': 0.0001},
          'Квадратный киллометр': {'Квадратный миллиметр': 1e+12, 'Квадратный сантиметр': 1e+10, 'Квадратный метр': 1000000, 'Гектар': 100},
          'Гектар': {'Квадратный миллиметр': 1e+10, 'Квадратный сантиметр': 1e+8, 'Квадратный метр': 10000, 'Квадратный киллометр': 0.01},

          'Бит': {'Байт': 0.125, 'Килобайт': 0.00012207, 'Мегабайт': 1.1921e-7, 'Гигабайт': 1.1642e-10, 'Терабайт': 1.1369e-13},
          'Байт': {'Бит': 8, 'Килобайт': 0.000976563, 'Мегабайт': 9.5367e-7, 'Гигабайт': 9.3132e-10, 'Терабайт': 9.0949e-13},
          'Килобайт': {'Бит': 8192, 'Байт': 1024, 'Мегабайт': 0.000976563, 'Гигабайт': 9.5367e-7, 'Терабайт': 9.3132e-10},
          'Мегабайт': {'Бит': 8388608, 'Байт': 1048576, 'Килобайт': 1024, 'Гигабайт': 0.000976563, 'Терабайт': 9.5367e-7},
          'Гигабайт': {'Бит': 8589934592, 'Байт': 1073741824, 'Килобайт': 1048576, 'Мегабайт': 1024, 'Терабайт': 0.000976563},
          'Терабайт': {'Бит': 8796093022208, 'Байт': 1099511627776, 'Килобайт': 1073741824, 'Мегабайт': 1048576, 'Гигабайт': 1024},
          'Петабайт': {'Бит': 9223372036854775808, 'Байт': 1125899906842624, 'Килобайт': 1099511627776, 'Мегабайт': 1073741824, 'Гигабайт': 1048576, 'Терабайт': 1024},

          'Грамм': {'Килограмм': 0.001, 'Тонна': 1e-6, 'Фунт': 0.00220462},
          'Килограмм': {'Грамм': 1000, 'Тонна': 0.001, 'Фунт': 2.20462},
          'Тонна': {'Грамм': 1e+6, 'Килограмм': 1000, 'Фунт': 2204.62},
          'Фунт': {'Грамм': 453.592, 'Килограмм': 0.453592, 'Тонна': 0.000453592},

          'Секунда': {'Минута': 0.0166667, 'Час': 0.000277778},
          'Минута': {'Секунда': 60, 'Час': 0.0166667},
          'Час': {'Секунда': 3600, 'Минута': 60},

          'Литр': {'Миллилитр': 1000, 'Галлон': 0.264172, 'Кубический сантиметр': 1000, 'Кубический метр': 0.001},
          'Миллилитр': {'Литр': 0.001, 'Галлон': 0.000264172, 'Кубический сантиметр': 1, 'Кубический метр': 0.000001},
          'Галлон': {'Литр': 3.78541, 'Миллилитр': 3785.41, 'Кубический сантиметр': 3785.41, 'Кубический метр': 0.00378541},
          'Кубический сантиметр': {'Литр': 0.001, 'Миллилитр': 1, 'Галлон': 0.000264172, 'Кубический метр': 0.000001},
          'Кубический метр': {'Литр': 1000, 'Миллилитр': 1000000, 'Галлон': 264.172, 'Кубический сантиметр': 1000000},

          'Метр в секунду': {'Километр в час': 3.6, 'Миля в час': 2.23694},
          'Километр в час': {'Метр в секунду': 0.277778, 'Миля в час': 0.621371},
          'Миля в час': {'Метр в секунду': 0.44704, 'Километр в час': 1.60934}
    };
  }
  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      navigationBar: CupertinoNavigationBar(
        backgroundColor: CupertinoColors.darkBackgroundGray,
        middle: Text(widget.category.name, style: const TextStyle(color: CupertinoColors.white)),
      ),
      backgroundColor: CupertinoColors.darkBackgroundGray,
      child: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              CupertinoTextField(
                placeholder: "Введите значение",
                keyboardType: TextInputType.number,
                onChanged: (value) {
                  setState(() {
                    inputValue = value;
                  });
                },
              ),
              const SizedBox(height: 16),
              const Text('Выберите единицу измерения, которую вы хотите конвертировать:', style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold, color: CupertinoColors.white)),
              const SizedBox(height: 8),
              SizedBox(
                height: 80, 
                child: CupertinoPicker(
                  itemExtent: 28.0, 
                  onSelectedItemChanged: (selectedIndex) {
                    setState(() {
                      selectedUnit = widget.category.units[selectedIndex];
                    });
                  },
                  children: widget.category.units.map((unit) => Text(unit)).toList(),
                ),
              ),
              const SizedBox(height: 16),
              Center(
                child: CupertinoButton.filled(
                  onPressed: convertValue,
                  child: const Text('Конвертировать'),
                ),
              ),
              const SizedBox(height: 16),
              const Text('Результаты конвертации:', style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold, color: CupertinoColors.white)),
              const SizedBox(height: 8),
              Expanded(
                child: ListView.builder(
                  itemCount: convertedValues.length,
                  itemBuilder: (context, index) {
                    String unit = convertedValues.keys.elementAt(index);
                    double value = convertedValues[unit]!;
                    return Center(child: Text('$value $unit', style: const TextStyle(fontSize: 24, color:CupertinoColors.white)));
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}